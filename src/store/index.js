import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    background: 'home',
    progress: {
      display: false,
      bar: 0,
      current: 0,
    },
    mascot: {
      display: false,
      title: '',
      text: '',
    },
    home: {
      mascot: {
        title: '',
        text: 'Prêt à t\'informer sur la pollution du numérique ?',
      },
    },
    miniGameQuestion: [
      {
        type: 'slider',
        min: 0,
        max: 200,
        sliderValue: 50,
        text: 'Combien as-tu de mails stockés dans ta messagerie en ce moment même?',
        answers: [
          {
            title: 'Les e-mails génèrent 410 millions de tonnes de C02 par an.',
            text: '- Au total, 281 milliards de mails ont été envoyés dans le monde, chaque jour,'
              + ' en 2018\n (cf. cabinet d\'études Radicati Group).',
            image: 'mailPoster',
            imageLeft: true,
          },
          {
            title: '',
            text: '- Envoyer une photo de vacances de 1 Mo à dix amis équivaut ainsi à parcourir '
              + '500 mètres en voiture.\n'
              + '- Même adressé à un collègue à quelques mètres de distance, votre e-mail envoie'
              + ' des données qui parcourent donc des milliers de kilomètres.',
            image: 'phone',
            imageLeft: false,
          },
          {
            title: 'Chaque Français stocke 10.000 à 50.000\n d\'e-mails inutilement.',
            text: '- Tous les courriels sont stockés dans des datacenters, qui consomment'
              + ' 200 TWh/an et produisent 0,3% des gaz à effet de serre.\n'
              + ' (cf. le site Nature & Edouard Nattée - Foxintelligence).',
            image: 'boxOfLetters',
            imageLeft: true,
          },
          {
            title: 'Comment réduire l\'empreinte carbone de ses e-mails ?',
            text: '- Réduire la taille des pièces jointes,'
              + ' ou envoyer un lien hypertexte plutôt qu\'un document\n'
              + ' - trier régulièrement sa boîte mail et éliminer tous les messages inutiles\n'
              + ' - utiliser une application qui nettoie sa boîte mail (Cleanfox)\n'
              + ' - se désabonner de toutes les newsletters ...',
            image: '',
          },
        ],
        mascotAnswers: [
          {
            title: 'C\'est bien, continue comme ça',
            text: 'Moins tu as de mails stockés, meilleur c\'est pour l\'environnement !',
            maxSliderValue: 100,
          },
          {
            title: 'Tu as beaucoup trop de mails stockés...',
            text: 'C\'est comme si tu avais laissé allumer la lumière du séjour pendant'
              + ' plusieurs heures.',
            maxSliderValue: 200,
          },
        ],
      },
      {
        type: 'slider',
        min: 0,
        max: 36,
        sliderValue: 10,
        text: 'Pour toi ... en moyenne, combien de mois dure le cycle de vie d\'un smartphone'
          + ' construit de nos jours ?',
        answers: [
          {
            title: 'L\'obsolescence programmée, vous saviez que c\'est rendre les appareils :\n'
              + 'Impossibles à réparer :',
            text: '- En fixant des pièces entre elles (aujourd’hui, les smartphones Samsung'
              + ' et LG ainsi que les ordinateurs portables Apple sont connues pour leurs batteries'
              + ' et pièces soudées entre elles (ou au boîtier).\n'
              + '- Soit en ne fournissant pas les pièces détachées nécessaires ou en rendant'
              + ' le prix de la réparation prohibitif (comme pour les lave-linge dont 80% sont'
              + ' dotés de cuves en plastique et de roulement à billes.'
              + ' Tant est si bien qu’ils tombent en panne au bout de'
              + ' 2 000 à 2 500 cycles de lavage).',
            image: '',
          },
          {
            title: 'Incompatibilité :',
            text: '- Lorsque certains nouveaux accessoires ne sont pas compatibles avec les'
              + ' anciens équipements (la connectique entre iPhone 4 et iPhone 5 par exemple).\n'
              + '- Ou lorsque certains logiciels ou systèmes d’exploitation refusent de'
              + ' fonctionner sur votre ancien équipement faute de place et de puissance.',
            image: '',
          },
          {
            title: 'Notifications trompeuses :',
            text: 'L’exemple le plus frappant est celui des imprimantes.\n'
              + 'En 2011, on découvre le message d’erreur d’une imprimante Epson alertant sur un'
              + ' tampon d’encre saturé. En réalité, une puce limitait le fonctionnement de'
              + ' l’appareil à 18 000 impressions pour inciter l’utilisateur à le remplacer.',
            image: 'imprimante',
          },
          {
            title: 'Comment éviter cela ?',
            text: 'Depuis 2015, en France, la loi sur la transition énergétique considère que'
              + ' l’obsolescence programmée est un délit. C’est pas mal, mais pas suffisant…',
            image: '',
          },
        ],
        mascotAnswers: [
          {
            title: 'C\'est 18 mois !',
            text: 'Savais-tu que mon NOKIA 3310 de 2000 fonctionnait encore ?',
          },
        ],
      },
      {
        type: 'radio',
        defaultChoice: 'Oui',
        choices: [
          {
            text: 'Oui',
            goodAnswer: false,
          },
          {
            text: 'Non',
            goodAnswer: true,
          },
        ],
        text: 'Est-ce que cela t\'est déjà arrivé de changer un appareil électroménager,'
          + ' car il n\'était plus écologique ?\n Ou un smartphone, une console, un ordinateur,'
          + ' car cet appareil venait juste de sortir ?',
        answers: [
          {
            title: 'L\'obsolescence programmée, c\'est aussi : Détourner \nl\'argument écologique',
            text: '- Inciter au remplacement d’objets en état de marche par d’autres moins'
              + ' consommateurs d’énergie et/ou plus respectueux de l’environnement'
              + ' (si l’idée est louable et permet de moins polluer durant l’utilisation de'
              + ' l’appareil, elle implique l’exploitation de ressources pour en fabriquer de'
              + ' nouveaux et la mise au rebut de ceux qui fonctionnent encore et'
              + ' dont il faut gérer la fin de vie.).',
            image: 'réusiné',
          },
          {
            title: 'Jouer sur l\'effet de mode',
            text: '- Les mouvements de foule et l’hystérie accompagnant la sortie d’une nouvelle'
              + ' console, d’un smartphone ou d’une collection capsule de prêt-à-porter,'
              + ' sont devenus légion. C’est un autre moyen de raccourcir la durée d’utilisation'
              + ' de nos objets : à grand renfort de lancements et de publicités, les marques'
              + ' donnent envie de posséder le modèle dernier cri même si on n\'en a pas besoin'
              + ' (par exemple, Apple sort plus d’un nouvel iPhone par an. C’est tentant…)',
            image: '',
          },
        ],
        mascotAnswers: [
          {
            title: '',
            text: 'Merci, j\'en apprends un petit peu plus sur toi',
          },
        ],
      },
      {
        type: 'slider',
        min: 0,
        max: 100,
        sliderValue: 25,
        text: 'Combien penses-tu avoir d’équipements électriques et électroniques'
          + ' chez toi, à la maison ?',
        answers: [
          {
            title: 'Le savais-tu ?…',
            text: 'Alors que les Français pensent posséder environ 34 équipements électriques et'
              + ' électroniques par foyer, ils en possèdent en réalité 99 !'
              + ' Et 6 d’entre eux ne sont JAMAIS utilisés.',
            image: '',
          },
          {
            title: '',
            text: 'Eh oui…la liste peut être longue : smartphone, appareils photo,'
              + ' l’électroménager, tablettes, jouets, box internet et ses applicatifs, drone,'
              + ' réveils, télécommandes, bureautique, rasoirs, consoles, matériels hifi,'
              + ' volets roulants, portails, calculatrices, appareils portatifs de bricolage,'
              + ' machine à coudre, etc.',
            image: 'conectique',
            imageLeft: true,
          },
          {
            title: 'Mais comment en est-on arrivé là ?\n',
            text: 'Grâce à la conjonction de 3 facteurs déterminants :\n'
              + '- L’obsolescence programmée\n'
              + '- Le marketing\n'
              + '- La surconsommation\n',
            image: 'marketing',
          },
        ],
        mascotAnswers: [
          {
            title: '',
            text: 'Merci pour ta franchise',
          },
        ],
      },
      {
        type: 'slider',
        min: 3,
        max: 50,
        sliderValue: 10,
        text: 'En moyenne, le poids total de tous les objets accumulés chez nous est de 2.5 tonnes'
          + ' (un gros hippopotame).\n'
          + 'Mais, d’après toi, quel est le poids caché (en tonnes) de l’ensemble des matières'
          + ' qu’il a fallu extraire, exploiter et transporter pour fabriquer ces objets ?',
        answers: [
          {
            title: 'On monte ainsi à 45 tonnes !!! Soit 8 gros éléphants !!!',
            text: '- l’exemple le plus parlant est le smartphone : pour récolter quelques grammes'
              + ' de minerais nécessaires à la fabrication de la puce qu’il renferme,'
              + ' il faut excaver 200 kg de matières.\n'
              + '- Par ailleurs,toutes les étapes du cycle de vie d’un objet, depuis l’extraction'
              + ' des matières premières qui le composent jusqu’à son élimination en fin de vie,'
              + ' génèrent des émissions de CO₂. C’est ce qu’on appelle le poids carbone.',
            image: 'minage',
          },
        ],
        mascotAnswers: [
          {
            title: '',
            text: 'Il y a beaucoup d’hippopotames dans ta maison dit-moi ?',
          },
        ],
      },
      {
        type: 'slider',
        min: 0,
        max: 500,
        sliderValue: 100,
        text: 'Selon toi quel est le poids carbone (en kg eq CO2) de toute la durée de vie d’une'
          + ' télévision de 30 à 40 pouces ?',
        answers: [
          {
            title: 'En moyenne la réponse est de 374 kg eq CO2.'
              + ' (l’équivalent d’un aller-retour en avion entre Paris et Nices).',
            text: 'Si on calcule ensemble :\n'
              + 'Production des matières premières et fabrication des composants = 299 kg eq CO2\n'
              + 'Approvisionnement = 10 kg eq CO2\n'
              + 'Mis en forme = 6 kg eq CO2\n'
              + 'Assemblage = 5 kg eq CO2\n'
              + 'Distribution = 23 kg eq CO2\n'
              + 'Utilisation = 41 kg eq CO2\n'
              + 'Fin de vie (recyclage) = - 10 kg eq CO2\n',
            image: 'fabrication',
          },
          {
            title: '',
            text: 'C’est vraiment la phase de production des matières premières et de fabrication'
              + ' des composants qui pèse le plus sur la planète. Plus les composants sont'
              + ' complexes, plus ils exigent des métaux rares : le tantale, par exemple,'
              + ' indispensable aux téléphones portables ; ou l\'indium, indispensable aux écrans'
              + ' plats LCD (la technologie « cristaux liquides », qui représente aujourd\'hui'
               + ' la plus grosse part du marché). Les fabricants sont en train d\'épuiser ces'
              + ' minerais précieux à un rythme inégalé afin d’améliorer ces impacts,'
              + ' as-tu entendu parler de “l’économie circulaire” ?',
            image: '',
          },
          {
            title: 'Continue la visite après ces jeux pour le découvrir.',
            text: '',
            image: '',
          },
        ],
        mascotAnswers: [
          {
            title: '',
            text: 'Wouaw… multiplié par le nombre de foyers de mon village…\n'
              + 'ça fait beaucoup… Et cela concerne uniquement les TV.',
          },
        ],
      },
      {
        type: 'radio',
        defaultChoice: 'Non',
        choices: [
          {
            text: 'Oui',
            goodAnswer: false,
          },
          {
            text: 'Non',
            goodAnswer: true,
          },
        ],
        text: 'Savais tu que la fabrication a un impact indirect mais réel sur les populations de pays qui sont loin de nous ?',
        answers: [
          {
            title: 'Impacts sur l’exploitation de la main-d’œuvre à bas coût',
            text: 'Comme la Chine par exemple, qui détient le monopole des terres rares,'
              + ' des minerais et métaux difficiles à extraire. De nombreuses entreprises'
              + ' du secteur, comme le groupe Foxconn, sous-traitant N°1 d’appareils électroniques'
              + ' pour Apple, Sony ou Dell, font travailler leurs ouvriers dans des conditions'
              + ' de travail épouvantables'
              + ' (onze suicides sur le lieu de travail entre janvier et juin 2010).',
            image: '',
          },
          {
            title: 'Sur la santé des habitants',
            text: 'Comme au Pérou, principal pays producteur de zinc ou de cuivre,'
              + ' où l’exploitation minière représente 60% des exportations, mais avec un'
              + ' lourd tribut à payer. À Cerro de Pasco, une mine de plomb et de '
              + 'zinc de 402 m de profondeur ronge la ville et la santé de ses habitants.',
            image: 'sante',
            imageLeft: true,
          },
          {
            title: 'Sur la stabilité politique',
            text: 'Dans nos téléphones,il y a toujours quelques grammes de la '
              + 'République démocratique du Congo. Dans la province de Kivu, on trouves des'
              + ' métaux qui entrent dans la fabrication de nos téléphones portables :'
              + ' tungstène, cassitérite,tantale et surtout du coltan. Gangrenée par une'
              + ' corruption endémique et par la convoitise des pays voisins, cette province'
              + ' est soumise à la domination de groupes armées ultra violents.',
            image: '',
          },
        ],
        mascotAnswers: [
          {
            title: '',
            text: 'Il est important d’y songer !',
          },
        ],
      },
      {
        type: 'radio',
        defaultChoice: 'Oui',
        choices: [
          {
            text: 'Oui',
            goodAnswer: false,
          },
          {
            text: 'Non',
            goodAnswer: true,
          },
        ],
        text: 'Pour acheter un appareil, Vous êtes vous déjà demandé :'
          + ' “Est-ce que j’en ai juste envie ? Ou en ai-je vraiment besoin ?”',
        answers: [
          {
            title: 'Quelquefois, on a juste envie de s’acheter quelque chose,'
              + ' c’est terrible mais ça arrive…\n Tant bien que mal, il faut'
              + ' identifier son besoin réel :',
            text: 'Soit c’est parce que cet objet nous apportera  une image, une'
              + ' reconnaissance sociale, ou même  du réconfort... Dans ces cas, posons-nous'
              + ' pour  réfléchir à une autre manière de les combler.\n'
              + 'Cet objet est nécessaire, utile, vous n’en  n’avez pas ou celui que vous'
              + ' aviez est absolument  irréparable ?',
            image: 'personWithPhone',
          },
          {
            title: '',
            text: 'Pour les télévisions, les smartphones, les tablettes les offres commerciales'
              + ' rivalisent de génie pour nous donner envie d’avoir des écrans plus grands…'
              + ' Or,sur une même catégorie de produit, le recours à des produits plus grands'
              + ' entraîne des contributions au changement climatique plus élevées en proportion.'
              + ' Ainsi, cette tendance du marché risque d’alourdir la contribution au changement'
              + ' climatique de nos objets connectés dans les années à venir.',
            image: '',
          },
          {
            title: 'Vous l’avez bien compris, les habitudes de consommation de chacun'
              + ' a une\n importance capitale pour les générations à venir.',
            text: '',
            image: '',
          },
        ],
        mascotAnswers: [
          {
            title: '',
            text: 'Je me pose souvent cette question pour remplacer mon téléphone NOKIA 3310...',
          },
        ],
      },
      {
        type: 'slider',
        min: 0,
        max: 10,
        sliderValue: 5,
        text: 'Lors de sa fabrication, combien de tour du monde a réalisé ton smartphone ?',
        answers: [
          {
            title: 'Quatre tours du monde pour fabriquer un smartphone',
            text: '-  Conception le plus souvent aux États-Unis\n'
              + '-  Extraction et transformation des matières premières en Asie du Sud-Est,'
              + ' en Australie, en Afrique centrale et en Amérique du Sud\n'
              + '-  Fabrication des principaux composants en Asie, aux États-Unis et en Europe\n'
              + '-  Assemblage en Asie du Sud-Est\n'
              + '- Distribution vers le reste du monde, souvent en avion.',
            image: 'tour_du_monde',
            imageLeft: true,
          },
          {
            title: 'En utilisant votre smartphone le plus longtemps possible, vous évitez'
              + ' la\n production de nouveaux appareils et vous préservez l’environnement'
              + ' et les populations !',
            text: '',
            image: '',
          },
        ],
        mascotAnswers: [
          {
            title: '',
            text: 'Oh… mes ailes ne me permettraient jamais un tel exploit.',
          },
        ],
      },
      {
        type: 'slider',
        min: 0,
        max: 5,
        sliderValue: 2,
        text: 'Selon toi ... quels est le poids carbone numérique'
          + ' (en Kg CO2) de ce module d’exposition pour une journée ?',
        answers: [
          {
            title: '',
            text: 'les tablettes étant ré-utilisées (occasion), ce module d’exposition génère'
              + ' moins de 1 Kg CO2 (étant donnée l’hébergement en DataCenter).\n'
              + 'Aucuns mails ni aucunes publicités ni aucunes newsletters ne sont envoyés.\n'
              + 'Aucunes données personnelles n’est stockées. \n'
              + 'Le nombre de page web et de requêtes est très limités.',
            image: 'ecologie',
          },
          {
            title: 'top 5 des data centers les plus écologiques :',
            text: 'Green Mountain Rennesoy (Norvège)\n'
              + 'son fonctionnement repose uniquement sur la consommation'
              + ' d’énergies renouvelables.\n'
              + 'EvoSwitch\n'
              + 'Grâce à la combinaison d’énergie éolienne, hydraulique et biomasse,'
              + ' ce data center et totalement alimenté et laisse présager de belles'
              + ' évolutions pour les futures générations.\n'
              + 'Marylin\n'
              + 'Sa consommation électrique est notamment optimisée par le biais de'
              + ' l’utilisation de l’air ambiant, ou encore du recyclage de chaleur'
              + ' pour chauffer les bureaux. Par conviction, ce data center n’utilise'
              + ' ni batteries au plomb, ni cuivre.\n'
              + 'Merlin (Royaume-Uni)\n'
              + 'un système de refroidissement à air frais qui permet d’économiser'
              + ' 80 % des coûts d’exploitation et qui produit jusqu’à 50 % moins'
              + ' d’émissions de carbone,\n'
              + 'une économie d’énergie de 91 % par rapport à un centre de données classique.\n'
              + 'Kolos (Norvège)\n'
              + 'Il sera alimenté à 100 % par de l’énergie durable et renouvelable,'
              + ' notamment éolienne et hydroélectrique en provenance des fjords qui'
              + ' entourent l’installation sur trois côtés. Le climat froid et le'
              + ' faible taux d’humidité de la Norvège contribueront également à'
              + ' compenser les besoins énergétiques.',
            image: '',
          },
        ],
        mascotAnswers: [
          {
            title: '',
            text: 'Merci pour vos applaudissements… c’est vrai que c’est peu !',
          },
        ],
      },
    ],
    end: {
      mascot: {
        title: '',
        text: 'Tu as finis et tu as compris comment diminuer les impacts sur l\'environnement !',
      },
    },
  },
  mutations: {
    setBackground(state, background) {
      state.background = background;
    },
    setProgress(state, progress) {
      state.progress = progress;
    },
    setMascot(state, mascot) {
      state.mascot = mascot;
    },
  },
  actions: {
  },
  modules: {
  },
});
