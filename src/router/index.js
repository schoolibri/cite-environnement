import Vue from 'vue';
import VueRouter from 'vue-router';
import Question from '../views/minigames/Question.vue';
import Home from '../views/Home.vue';
import End from '../views/End.vue';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home,
  },
  {
    path: '/question',
    name: 'Question',
    component: Question,
  },
  {
    path: '/end',
    name: 'End',
    component: End,
  },
  {
    path: '*',
    redirect: '/',
  },
];

const router = new VueRouter({
  mode: 'history',
  routes,
});

export default router;
